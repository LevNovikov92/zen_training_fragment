package training.zm.lev.trainingproj;

import android.app.FragmentTransaction;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import training.zm.lev.trainingproj.fragments.NotesDescFragment;
import training.zm.lev.trainingproj.fragments.NotesTitleFragment;
import training.zm.lev.trainingproj.models.Note;


public class NotesActivity extends ActionBarActivity implements
        NotesTitleFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);

        if(savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            fragmentTransaction.add(R.id.noteTitleFrame, new NotesTitleFragment());
            fragmentTransaction.add(R.id.noteDescFrame, new NotesDescFragment());
            fragmentTransaction.commit();
        }


        // Uncomment next 3 lines to set test data
        //DataBaseHelper dbh = new DataBaseHelper(this);
        //SQLiteDatabase db = dbh.getReadableDatabase();
       // setTestData(db);
        //db.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_news, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    private void setTestData(SQLiteDatabase db) {
        Note note1 = new Note();
        note1.setProperty(Note.TITLE_COLUMN, "note 1 title");
        note1.setProperty(Note.DESC_COLUMN, "note 1 desc desc desc desc");
        note1.save(db);
        Note note2 = new Note();
        note2.setProperty(Note.TITLE_COLUMN, "note 2 title");
        note2.setProperty(Note.DESC_COLUMN, "note 2 desc desc desc desc");
        note2.save(db);
        Note note3 = new Note();
        note3.setProperty(Note.TITLE_COLUMN, "note 3 title");
        note3.setProperty(Note.DESC_COLUMN, "note 3 desc desc desc desc");
        note3.save(db);
    }

    @Override
    public void onTitleFragmentInteraction(int noteId) {
        Toast.makeText(this, "onTitleFragment  id=" + noteId, Toast.LENGTH_SHORT).show();
        NotesDescFragment desc = NotesDescFragment.newInstance(noteId);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        //ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_in_right);
        ft.replace(R.id.noteDescFrame, desc);
        ft.commit();
    }
}
