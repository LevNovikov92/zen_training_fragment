/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package training.zm.lev.trainingproj.orm.properties;

/**
 *
 * @author NovikovLU
 */
public abstract class Property {
    
    public final String name;
    public final String type;
    
    public Property(String name) {
        this.name = name;
        this.type = this.getClass().getSimpleName();
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type.toString();
    }

    public abstract Object getValue();
    
    public abstract void setValue(int value);

    public abstract void setValue(Integer value);
    
    public abstract void setValue(String value);

    public abstract void setValue(float value);

}
