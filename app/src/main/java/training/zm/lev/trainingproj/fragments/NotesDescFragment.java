package training.zm.lev.trainingproj.fragments;

import android.app.Fragment;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import training.zm.lev.trainingproj.DataBaseHelper;
import training.zm.lev.trainingproj.R;
import training.zm.lev.trainingproj.models.Note;
import training.zm.lev.trainingproj.orm.Condition;

public class NotesDescFragment extends Fragment {

    public static NotesDescFragment newInstance(Integer noteId) {
        NotesDescFragment fragment = new NotesDescFragment();

        Bundle bundle = new Bundle();
        bundle.putInt("noteId", noteId);
        fragment.setArguments(bundle);
        return fragment;
    }

    public NotesDescFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_notes_desc, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.noteDescTextView);
        Condition cnd = new Condition();
        int curNoteId = 0;
        if(getArguments()!=null) {
            System.out.println(savedInstanceState);
            Integer noteId = getArguments().getInt("noteId");
            if(noteId!=null) {
                curNoteId = noteId;
            }
            if(savedInstanceState!=null) {
                curNoteId = savedInstanceState.getInt("curNoteId", 0);
                System.out.println(curNoteId);
            }
            if(curNoteId!= 0) {
                cnd.selection = "_id="+curNoteId;
                DataBaseHelper dbHelper = new DataBaseHelper(getActivity().getApplicationContext());
                SQLiteDatabase db = dbHelper.getReadableDatabase();
                Note note = (Note) Note.model().findOne(cnd, db);
                textView.setText((String) note.getProperty(Note.DESC_COLUMN));
                db.close();
            }
        }

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(getArguments()!=null) {
            Integer noteId = getArguments().getInt("noteId");
            if(noteId!=null)
                outState.putInt("curNoteId", noteId);
            Log.e("DEV", "OnSaveInst");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
