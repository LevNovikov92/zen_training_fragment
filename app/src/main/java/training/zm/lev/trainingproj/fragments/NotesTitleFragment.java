package training.zm.lev.trainingproj.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import training.zm.lev.trainingproj.DataBaseHelper;
import training.zm.lev.trainingproj.NotesAdapter;
import training.zm.lev.trainingproj.R;
import training.zm.lev.trainingproj.models.Note;
import training.zm.lev.trainingproj.orm.Condition;
import training.zm.lev.trainingproj.orm.Model;


public class NotesTitleFragment extends Fragment {

    ArrayList<Model> notes;
    RecyclerView mRecyclerView = null;

    private OnFragmentInteractionListener mListener;

    public static NotesTitleFragment newInstance() {
        NotesTitleFragment fragment = new NotesTitleFragment();
        return fragment;
    }

    public NotesTitleFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_notes_title, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplication().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        DataBaseHelper dbHelper = new DataBaseHelper(getActivity().getApplication().getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Condition cnd = new Condition();
        notes = Note.model().find(cnd,db);
        db.close();
        NotesAdapter mAdapter = new NotesAdapter(notes, this);
        mRecyclerView.setAdapter(mAdapter);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onTitleFragmentInteraction(int noteTitle);
    }

    public void onClick(int noteId) {
        if (mListener != null) {
            mListener.onTitleFragmentInteraction(noteId);
        }
    }

}
