package training.zm.lev.trainingproj.models;

import android.provider.BaseColumns;

import training.zm.lev.trainingproj.orm.Model;

/**
 * Created by Lev on 02.01.2016.
 */
public class Note extends Model {

    public static final String ID_COLUMN = "_id";
    public static final String TITLE_COLUMN = "title";
    public static final String DESC_COLUMN = "desc";


    public static final String TABLE_CREATE_SCRIPT = "create table "
            + "'note'" + " (" + BaseColumns._ID
            + " integer primary key autoincrement, "
            + TITLE_COLUMN + " text not null, "
            + DESC_COLUMN + " text not null "
            + ");";

    @Override
    public void init() {
        setTableName("note");
        addIntProperty("_id");
        addStringProperty("title");
        addStringProperty("desc");
    }

    @Override
    public Note getInstance() {
        return new Note();
    }

    @Override
    public String getTableCreateScript() {
        return TABLE_CREATE_SCRIPT;
    }

    public static Note model() {
        return new Note();
    }
}
