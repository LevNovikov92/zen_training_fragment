/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package training.zm.lev.trainingproj.orm.properties;

/**
 *
 * @author NovikovLU
 */
public class IntProperty extends Property {
    
    public Integer value;

    public IntProperty(String name) {
        super(name);
    }
    
    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public void setValue(String value) {
        throw new UnsupportedOperationException("Invalid Value type"); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setValue(float value) {
        throw new UnsupportedOperationException("Invalid Value type"); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setValue(int value) {
        throw new UnsupportedOperationException("Invalid Value type"); //To change body of generated methods, choose Tools | Templates.
    }


}
