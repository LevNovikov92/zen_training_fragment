package training.zm.lev.trainingproj;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import training.zm.lev.trainingproj.fragments.NotesTitleFragment;
import training.zm.lev.trainingproj.models.Note;
import training.zm.lev.trainingproj.orm.Model;

/**
 * Created by Lev on 02.01.2016.
 */
public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {

    public ArrayList<Model> Notes;
    public NotesTitleFragment fragment;

    public NotesAdapter(ArrayList<Model> data, NotesTitleFragment fragment) {
        Notes = data;
        this.fragment = fragment;
    }

    @Override
    public NotesAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notes_list_item, viewGroup, false);
        ViewHolder vh = new ViewHolder(view, Notes, fragment);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder vh, int i) {
        Model note = Notes.get(i);
        vh.mTextView.setText((String) note.getProperty(Note.TITLE_COLUMN));
    }

    @Override
    public int getItemCount() {
        return Notes.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        // each data item is just a string in this case
        private Context context;
        private  ArrayList<Model> notes;
        private NotesTitleFragment fragment;

        public TextView mTextView;
        public ViewHolder(View v, ArrayList<Model> notes, NotesTitleFragment fragment) {
            super(v);
            this.notes = notes;
            this.fragment = fragment;
            v.setClickable(true);
            v.setOnClickListener(this);
            context = v.getContext();
            mTextView = (TextView) v.findViewById(R.id.noteTextView);
        }


        @Override
        public void onClick(View v) {
            fragment.onClick((Integer) notes.get(getPosition()).getProperty("_id"));
            v.setActivated(true);

            //Toast.makeText(context, "Item "+getPosition() + " ID " + notes.get(getPosition()).getProperty("_id"), Toast.LENGTH_SHORT).show();
        }
    }
}
