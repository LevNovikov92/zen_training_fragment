package training.zm.lev.trainingproj;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import training.zm.lev.trainingproj.fragments.ButtonBarFragment;


public class TestFragmentActivity extends ActionBarActivity implements ButtonBarFragment.OnFragmentInteractionListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_fragment);

        Button buttonAddFragment = (Button) findViewById(R.id.buttonAddFragment);
        buttonAddFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_in_right);
                fragmentTransaction.add(R.id.buttonBarLayout, new ButtonBarFragment());
                fragmentTransaction.commit();
            }
        });

        Button buttonDeleteFragment = (Button) findViewById(R.id.buttonDeleteFragment);
        buttonDeleteFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                Fragment fragment = (Fragment) getFragmentManager().findFragmentById(R.id.buttonBarLayout);
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_in_right);
                if (fragment != null)
                    fragmentTransaction.remove(fragment);
                fragmentTransaction.commit();
            }
        });

        final Button addListenerButton = (Button) findViewById(R.id.buttonAddListener);
        addListenerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ButtonBarFragment buttonBarFragment = (ButtonBarFragment) getFragmentManager().findFragmentById(R.id.buttonBarLayout);
                if (buttonBarFragment != null) {
                    View view = buttonBarFragment.getView();
                    Button prevButton = (Button) view.findViewById(R.id.prevButton);
                    prevButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast toast = Toast.makeText(getApplicationContext(), "Prev button click!", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
