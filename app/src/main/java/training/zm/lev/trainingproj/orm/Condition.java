/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package training.zm.lev.trainingproj.orm;

/**
 *
 * @author NovikovLU
 */
public class Condition {
    public String[] columns = null;
    
    public String selection = null;
    
    public String[] selectionArgs = null;
    
    public String groupBy = null;
    
    public String having = null;
    
    public String orderBy = null;

    public String limit = null;
}
