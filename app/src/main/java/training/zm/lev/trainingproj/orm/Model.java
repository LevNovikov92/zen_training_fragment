/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package training.zm.lev.trainingproj.orm;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.JsonReader;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import training.zm.lev.trainingproj.orm.properties.FloatProperty;
import training.zm.lev.trainingproj.orm.properties.IntProperty;
import training.zm.lev.trainingproj.orm.properties.Property;
import training.zm.lev.trainingproj.orm.properties.StringProperty;

/**
 *
 * @author NovikovLU
 */
public abstract class Model {
    public HashMap<String, Property> Properties;
    
    private static String TableName;

    public Model() {
        Properties = new HashMap<String, Property>();
        init();
    }

    public abstract void init();

    public HashMap<String, Property> getProperties() {
        return Properties;
    }

    public void setProperties(HashMap<String, Property> Properties) {
        this.Properties = Properties;
    }
    
    public final void addIntProperty(String name) {
        Properties.put(name, new IntProperty(name));
    }

    public final void addFloatProperty(String name) {
        Properties.put(name, new FloatProperty(name));
    }
    
    public final void addStringProperty(String name) {
        Properties.put(name, new StringProperty(name));
    }
    
    public Object getProperty(String name) {
        Property property = Properties.get(name);
        if(property!=null)
            return property.getValue();
        else {
            Log.e("getProperty error", "Property " + name + " not found");
            return null;
        }
    }
    
    public void setProperty(String name, String value) {
        try {
            Properties.get(name).setValue(value);
        } catch (UnsupportedOperationException e) {
            System.err.println(e);
        } catch (NullPointerException e) {
            System.err.println(e + ":  Undefined column name");
        }
    }
    
    public void setProperty(String name, int value) {

        try {
            Properties.get(name).setValue(value);
        } catch (UnsupportedOperationException e) {
            System.err.println(e);
        } catch (NullPointerException e) {
            System.err.println(e + ":  Undefined column name");
        }
    }

    public void setProperty(String name, Integer value) {

        try {
            Properties.get(name).setValue(value);
        } catch (UnsupportedOperationException e) {
            System.err.println(e);
        } catch (NullPointerException e) {
            System.err.println(e + ":  Undefined column name");
        }
    }

    public void setProperty(String name, float value) {

        try {
            Properties.get(name).setValue(value);
        } catch (UnsupportedOperationException e) {
            System.err.println(e);
        } catch (NullPointerException e) {
            System.err.println(e + ":  Undefined column name");
        }
    }
    
    public ArrayList<Model> find(Condition condition, SQLiteDatabase db) {
        Cursor cursor = db.query("'"+TableName+"'", condition.columns,
                condition.selection,
                condition.selectionArgs,
                condition.groupBy,
                condition.having,
                condition.orderBy,
                condition.limit
        );

        ArrayList<Model> models = new ArrayList<>();

        while (cursor.moveToNext()) {
            Model model = this.getInstance();
            for (Map.Entry<String, Property> entry : Properties.entrySet()) {
                switch (entry.getValue().getType()) {
                    case "IntProperty":
                        Integer val = new Integer(cursor.getInt(cursor.getColumnIndex(entry.getKey())));
                        model.setProperty(entry.getKey(), val);
                        break;
                    case "StringProperty":
                        model.setProperty(entry.getKey(), cursor.getString(cursor.getColumnIndex(entry.getKey())));
                        break;
                    case "FloatProperty":
                        model.setProperty(entry.getKey(),(float) cursor.getDouble(cursor.getColumnIndex(entry.getKey())));
                        break;
                }
            }
            models.add(model);
            System.out.println(model.getProperty("title"));
            System.out.println(model.getProperty("_id"));
        }
        cursor.close();
        return models;
    }

    public Model findOne(Condition condition, SQLiteDatabase db) {
        Cursor cursor = db.query("'"+TableName+"'", condition.columns,
                condition.selection,
                condition.selectionArgs,
                condition.groupBy,
                condition.having,
                condition.orderBy,
                condition.limit
        );
        cursor.moveToNext();
            Model model = this.getInstance();
            for (Map.Entry<String, Property> entry : Properties.entrySet()) {
                switch (entry.getValue().getType()) {
                    case "IntProperty":
                        Integer val = new Integer(cursor.getInt(cursor.getColumnIndex(entry.getKey())));
                        model.setProperty(entry.getKey(), val);
                        break;
                    case "StringProperty":
                        model.setProperty(entry.getKey(), cursor.getString(cursor.getColumnIndex(entry.getKey())));
                        break;
                    case "FloatProperty":
                        model.setProperty(entry.getKey(),(float) cursor.getDouble(cursor.getColumnIndex(entry.getKey())));
                        break;
                }
        }
        cursor.close();
        return model;
    }

    public static String getTableName() {
        return TableName;
    }

    public static void setTableName(String tableName) { TableName = tableName;}

    public static String getSQLTableName() { return "'"+TableName+"'"; }

    public abstract Model getInstance();

    public Model getModelFromJsonReader(JsonReader reader) {
        try {
            reader.beginObject();
            Model model = this.getInstance();
            HashMap<String, Property> properties = this.getProperties();
            while(reader.hasNext())
            {
                String name = reader.nextName();

                if(properties.containsKey(name)) {
                    String type = properties.get(name).getType();
                    switch (type) {
                        case "StringProperty":
                            model.setProperty(name, reader.nextString());
                            break;
                        case "IntProperty":
                            model.setProperty(name, reader.nextInt());
                            break;
                        case "FloatProperty":
                            model.setProperty(name, (float) reader.nextDouble());
                            break;
                    }
                } else
                    reader.skipValue();
            }
            reader.endObject();
            return model;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Model getFromJSON(String json) {
        try {
            JSONObject reader = new JSONObject(json);
            Model model = this.getInstance();
            for (Map.Entry<String, Property> entry : Properties.entrySet()) {
                switch (entry.getValue().getType()) {
                    case "IntProperty":
                        model.setProperty(entry.getKey(), reader.getInt(entry.getKey()));
                        break;
                    case "StringProperty":
                        model.setProperty(entry.getKey(), reader.getString(entry.getKey()));
                        break;
                    case "FloatProperty":
                        model.setProperty(entry.getKey(), Float.valueOf(reader.getString(entry.getKey())));
                        break;
                }
            }
            return model;
        }  catch (JSONException e) {
            e.printStackTrace();
            return  null;
        }
    }

    public boolean save(SQLiteDatabase db)
    {
        String columns = "";
        String values = "";
        int count = 0;
        for (Map.Entry<String, Property> entry : Properties.entrySet()) {
            String column = entry.getKey();
            String value = "";
            if(entry.getValue().getValue() == null) {
                count++;
                continue;
            }

            switch (entry.getValue().getType()) {
                case "IntProperty":
                    value = entry.getValue().getValue().toString();
                    break;
                case "StringProperty":
                    value = (String) entry.getValue().getValue();
                    value = "'" + value + "'";
                    break;
                case "FloatProperty":
                    value =  entry.getValue().getValue().toString();
                    value = "'" + value + "'";
                    break;
            }
            if(entry.getValue().getValue()!=null) {
                columns += column;
                values += value;
            } else
                continue;

            if(count<Properties.size()-1)
            {
                count++;
                columns += ", ";
                values += ", ";
            }
        }
        String insertQuery = "INSERT INTO " +
                getSQLTableName() +
                " (" + columns + ") VALUES (" + values + ")";
        System.out.println(insertQuery);
        try{
            db.execSQL(insertQuery);
        }catch (SQLException e) {
            Log.e("SQL Exception", e.toString());
            return false;
        } catch (Exception e) {
            Log.e("ERR", e.toString());
            return false;
        }
        return true;
    }

    public abstract String getTableCreateScript();
}
